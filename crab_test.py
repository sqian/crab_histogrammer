#!/usr/bin/env python
from PhysicsTools.NanoAODTools.postprocessing.framework.postprocessor import PostProcessor

from PhysicsTools.NanoAODTools.postprocessing.framework.crabhelper import inputFiles, runsAndLumis
from array import array

from PhysicsTools.NanoAODTools.postprocessing.modules.test import ExampleAnalysis

preselection = "nJet > 1 && nMuon > 1"
# files = ["original_nanoaod.root"]
p = PostProcessor(".", inputFiles(), cut=preselection, branchsel=None, modules=[
                  ExampleAnalysis()], noOut=True, histFileName="histOut.root", histDirName="plots")
p.run()

print("DONE")
